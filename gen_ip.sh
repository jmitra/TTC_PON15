qsys_gen="/opt/altera/15.1/quartus/sopc_builder/bin/qsys-generate"

$qsys_gen ./Arria10/ATX_PLL_RESET.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/ATX_PLL_RESET --family="Arria 10" --part=10AX115N4F40I3SGE2
$qsys_gen ./Arria10/ISSP.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/ISSP --family="Arria 10" --part=10AX115N4F40I3SGE2
$qsys_gen ./Arria10/TX_ATX_PLL.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/TX_ATX_PLL --family="Arria 10" --part=10AX115N4F40I3SGE2
$qsys_gen ./Arria10/delay_insert_pll.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/delay_insert_pll --family="Arria 10" --part=10AX115N4F40I3SGE2
$qsys_gen ./Arria10/phase_sample_clk_fpll.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/phase_sample_clk_fpll --family="Arria 10" --part=10AX115N4F40I3SGE2

$qsys_gen ./Arria10/Tx_PHY_RESET.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/Tx_PHY_RESET --family="Arria 10" --part=10AX115N4F40I3SGE2
$qsys_gen ./Arria10/FPLL_CASCADE_SOURCE.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/FPLL_CASCADE_SOURCE --family="Arria 10" --part=10AX115N4F40I3SGE2
$qsys_gen ./Arria10/Rx_PHY_RESET.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/Rx_PHY_RESET --family="Arria 10" --part=10AX115N4F40I3SGE2
$qsys_gen ./Arria10/TX_RX_PHY.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/TX_RX_PHY --family="Arria 10" --part=10AX115N4F40I3SGE2

$qsys_gen ./Arria10/IOPLL.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/IOPLL --family="Arria 10" --part=10AX115N4F40I3SGE2
$qsys_gen ./Arria10/temp_measure_ip.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./Arria10/temp_measure_ip --family="Arria 10" --part=10AX115N4F40I3SGE2

