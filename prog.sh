quartus_pgm="/opt/altera/15.1/quartus/bin/quartus_pgm"
jtagconfig="/opt/altera/15.1/quartus/bin/jtagconfig"

qpf_file="Arria10_transceiver_test"
qsf_file="Arria10_transceiver_test"

echo Adding Jtag Server
$jtagconfig --addserver crorcdev a10gx
echo Enumerating Jtag Links
$jtagconfig --enum
time $quartus_pgm  -m JTAG -o "\"p\;./output_files/"$qsf_file.sof\"

