onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/RESET
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/REF_CLK
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/tx_serial_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/rx_serial_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/GENERAL_RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TX_PCS_RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/RX_PCS_RESET_I
add wave -noupdate /ttc_pon_top_tb/TTC_PON_TOP_comp/TX_PHY_RESET_I
add wave -noupdate /ttc_pon_top_tb/TTC_PON_TOP_comp/RX_PHY_RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TX_PMA_WORD_CLK_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/RX_PMA_WORD_CLK_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/tx_serial_clk_o
add wave -noupdate  /ttc_pon_top_tb/TTC_PON_TOP_comp/sources
add wave -noupdate -divider TX_RX_PHY
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/reconfig_clk
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/reconfig_reset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_analogreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_cdr_refclk0
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_clkout(0) -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_clkout(0) {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_clkout
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_coreclkin(0) -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_coreclkin(0) {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_coreclkin
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_digitalreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_is_lockedtodata
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_is_lockedtoref
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_parallel_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_pma_clkslip
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_serial_data
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_seriallpbken(0) -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_seriallpbken(0) {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_seriallpbken
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_analogreset(0) -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_analogreset(0) {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_analogreset
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(5) -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(4) -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(3) -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(2) -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(1) -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(0) -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(5) {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(4) {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(3) {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(2) {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(1) {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(0) {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_clkout
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_coreclkin
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_digitalreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_parallel_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_serial_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/unused_rx_parallel_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/unused_tx_parallel_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/xcvr_native_a10_0_rx_parallel_data
add wave -noupdate -divider TX_ATX_PLL
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/mcgb_rst
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/pll_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/pll_locked
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/pll_powerdown
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/pll_refclk0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_write0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_read0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_address0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_writedata0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_readdata0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_waitrequest0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_clk0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_reset0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/tx_bonding_clocks
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/tx_serial_clk
add wave -noupdate -divider TX_PHY_RESET
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/clock
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/pll_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/pll_locked
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/pll_select
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/reset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/tx_analogreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/tx_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/tx_digitalreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/tx_ready
add wave -noupdate -divider RX_PHY_RESET
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/clock
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/reset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/rx_analogreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/rx_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/rx_digitalreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/rx_is_lockedtodata
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/rx_ready
add wave -noupdate -divider ATX_PLL_RESET
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/ATX_PLL_RESET_comp/clock
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/ATX_PLL_RESET_comp/pll_powerdown
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/ATX_PLL_RESET_comp/reset
add wave -noupdate -divider PCS
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TX_PCS_RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/RX_PCS_RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TX_PMA_WORD_CLK_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/RX_PMA_WORD_CLK_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TX_RESET
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/RX_RESET
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_O
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_O.rx_frame_o
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.cal_ena_i -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.fast_beat_i -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.slow_beat_i -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.frame_ena_i -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.tx_data_payload_i -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.onu_address_i -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.rx_parallel_data_i -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.rx_phy_ready_i -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.onu_word_aligner_enable_i -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.cal_ena_i {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.fast_beat_i {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.slow_beat_i {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.frame_ena_i {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.tx_data_payload_i {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.onu_address_i {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.rx_parallel_data_i {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.rx_phy_ready_i {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I.onu_word_aligner_enable_i {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TTC_PCS_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/enc_8b10b_data_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/dec_8b10b_data_o
add wave -noupdate -divider WORD_ALIGNER
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/ENABLE_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/LOGIC_CLK_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/RX_WORD_CLK_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/CLOCK_ENABLE_O
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/mod_6_word_position_counter
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/RX_DATA_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/RESET_PHY_O
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/BITSLIP_O
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/ALIGNED_O
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/BITSLIP_NO_O
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/WORD_ALIGNER_STATE_O
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/s_word_aligner_FSM_state
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/s_algined
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/s_bitslip_counter
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/s_lock_tolerance_counter
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/s_search_tolerance_counter
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/s_is_comma_detected
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/onu_word_align_comp/s_is_frame_aligned
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {154193574 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 372
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {130979719 ps} {159961521 ps}
run 100ns
force -freeze sim:/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/GENERAL_RESET_I 1 0 -cancel 1000
force -freeze sim:/ttc_pon_top_tb/TTC_PON_TOP_comp/sources(10) 1 0
run 5000ns
force -freeze sim:/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/GENERAL_RESET_I 1 0 -cancel 1000
run -all