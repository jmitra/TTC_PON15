-------------------------------------------------------------------------------
-- Title      : PRBS Pattern Generator and Checker Test Bench
-- Project    : TTC-PON 2015 for Arria-10
-------------------------------------------------------------------------------
-- File       : prbs_data_gen_check_tb.vhd
-- Author     : Jubin MITRA
-- Company    : 
-- Created    : 16-03-2016
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- input  clock
-- input phase shifted clock
-- sample clock			
-- output integer part 		
-- output fractional part
-- output decimal part			
------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 16-03-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;


--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity prbs_data_gen_check_tb is

end prbs_data_gen_check_tb;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of prbs_data_gen_check_tb is
 
--=================================================================================================--
						--========####  Component Declaration  ####========-- 
--=================================================================================================--   

component prbs_data_gen  is
    generic (
        G_DOUT_WIDTH   : integer := 32;
        G_PRBS_TAP     : integer := 0;
        G_REVERSE_BITS : integer := 0);
    port (
        clock_i : in std_logic;
        mode_i  : in std_logic;  -- '0' external seed, '1' internal loop-back
        ena_i   : in std_logic;  -- '0' hold, '1' generate
        sel_i   : in std_logic_vector(1 downto 0);
        -- "00" PRBS-7, "01" PRBS-23
        -- "1x" fix
        seed_i  : in std_logic_vector(22 downto 0);
        dout_o  : out std_logic_vector(G_DOUT_WIDTH-1 downto 0));

end component ;
	
component prbs_data_check  is
    generic (
        G_DIN_WIDTH    : integer := 32;
		G_REVERSE_BITS : integer := 0);
    port (
		clock_i           : in  std_logic;
		reset_i           : in  std_logic;
        ena_i             : in  std_logic;
        sel_i             : in  std_logic_vector(1 downto 0);
        -- "00" PRBS-7, "01" PRBS-23
        -- "1x" fix
		din_i             : in  std_logic_vector(G_DIN_WIDTH-1 downto 0);
		lock_o            : out std_logic;
		error_o           : out std_logic;
		nerrs_o           : out std_logic_vector(7 downto 0);
		refgen_o          : out std_logic_vector(G_DIN_WIDTH-1 downto 0));

end component ;

--=================================================================================================--
						--========####  Signal Declaration  ####========-- 
--=================================================================================================--   
signal RESET									: std_logic:='0';
signal REF_CLK									: std_logic:='0';

signal MODE 									: std_logic:='0';
signal ENABLE									: std_logic:='0';
signal SELECT_PRBS								: std_logic_vector(1 downto 0):="00";
signal LOCK     								: std_logic:='0';
signal ERROR     								: std_logic:='0';

signal SEED     								: std_logic_vector( 22 downto 0):=(others=>'0');
signal PRBS_DATA_OUT                   	        : std_logic_vector( 31 downto 0);
signal PRBS_DATA_IN                    	        : std_logic_vector( 31 downto 0);
signal NO_OF_ERRORS                    	        : std_logic_vector( 7 downto 0);
signal REF_GEN                       	        : std_logic_vector( 31 downto 0);


constant refclk_period	 						: time := 8.333   ns;			
constant wait_period		 					: time := 10  ns;			

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--  
   
   --==================================== Port Mapping ======================================--
    prbs_data_gen_comp:
    prbs_data_gen
        generic map (
            G_DOUT_WIDTH   => 32,
            G_PRBS_TAP     => 0,
            G_REVERSE_BITS => 0)
        port map(
            clock_i        => REF_CLK,
            mode_i         => MODE,        -- '0' external seed, '1' internal loop-back
            ena_i          => ENABLE,      -- '0' hold, '1' generate
            sel_i          => SELECT_PRBS, -- "00" PRBS-7, "01" PRBS-23, "1x" fix
            seed_i         => SEED,
            dout_o         => PRBS_DATA_OUT
            );

    prbs_data_check_comp:
    prbs_data_check 
        generic map (
            G_DIN_WIDTH       => 32,
            G_REVERSE_BITS    => 0)
        port map (
            clock_i           => REF_CLK,
            reset_i           => RESET,
            ena_i             => ENABLE,        -- '0' hold, '1' generate
            sel_i             => SELECT_PRBS,   -- "00" PRBS-7, "01" PRBS-23, "1x" fix
            din_i             => PRBS_DATA_IN,
            lock_o            => LOCK,
            error_o           => ERROR,
            nerrs_o           => NO_OF_ERRORS,
            refgen_o          => REF_GEN
            );

    --==================================== Clock Generation =====================================--

   refclk_proc: 
   process
   begin
		
		REF_CLK <= '1';
		wait for refclk_period/2;
		REF_CLK <= '0';
		wait for refclk_period/2;
   end process;

   --==================================== User Logic =====================================--
   -- Error Injector
   PRBS_DATA_IN        <= PRBS_DATA_OUT ;--xor x"0001_1001";
   
	test_proc:
	process
	begin
		RESET           <=  '1';
        ENABLE          <=  '0';
        SELECT_PRBS     <= "00";
        SEED            <=  "111" & x"F_FFFF";
        MODE            <=  '0';
		wait for wait_period;
		RESET           <=  '0';
		wait for wait_period*10;
        ENABLE          <=  '1';
        MODE            <=  '1';
		wait for wait_period*10;
		wait;

	end process;
		
   --=====================================================================================--     
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--